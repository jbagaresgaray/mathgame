 var starter = starter || {};

 starter.Controllers = angular.module('starter.controllers', []);
 starter.Services = angular.module('starter.services', []);
 starter.Directive = angular.module('starter.directive', []);
 starter.Values = angular.module('starter.values', []);


 angular.module('starter', ['ionic', 'ngCordova', 'ngDraggable','starter.controllers', 'starter.services', 'starter.values'])

 .run(function($ionicPlatform) {
         $ionicPlatform.ready(function() {
             if (window.cordova && window.cordova.plugins.Keyboard) {
                 cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
             }
             if (window.StatusBar) {
                 StatusBar.styleDefault();
             }
         });
     })
     .run(function(DB) {
         DB.init();
     })
     .constant('DB_CONFIG', {
         name: 'DB',
         tables: [{
             name: 'scores',
             columns: [{
                 name: 'id',
                 type: 'integer primary key AUTOINCREMENT'
             }, {
                 name: 'topicId',
                 type: 'text'
             }, {
                 name: 'name',
                 type: 'text'
             }, {
                 name: 'score',
                 type: 'text'
             }]
         }]
     })
     .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
         $ionicConfigProvider.backButton.text('');
         $ionicConfigProvider.backButton.previousTitleText(false);

         $stateProvider
             .state('quizans', {
                 url: "/quizans",
                 templateUrl: "templates/quiz.html",
                 controller: 'QuizCtrl'
             })
             .state('score', {
                 url: "/score",
                 templateUrl: "templates/score.html",
                 controller: 'ScoreCtrl'
             })
             .state('drag', {
                 url: "/drag",
                 templateUrl: "templates/drag.html",
                 controller: 'DragDropCtrl'
             })
             .state('dragscore', {
                 url: "/dragscore",
                 templateUrl: "templates/dragscore.html",
                 controller: 'ScoreCtrl'
             })
             .state('counting', {
                 url: "/counting",
                 templateUrl: "templates/counting.html",
                 controller: 'CountingCtrl'
             })
             .state('countingscore', {
                 url: "/countingscore",
                 templateUrl: "templates/countingscore.html",
                 controller: 'ScoreCtrl'
             })
             .state('arrange', {
                 url: "/arrange",
                 templateUrl: "templates/arrange.html",
                 controller: 'ArrangeCtrl'
             })
             .state('app', {
                 url: "/app",
                 abstract: true,
                 templateUrl: "templates/menu.html",
                 controller: 'AppCtrl'
             })
             .state('app.search', {
                 url: "/search",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/search.html"
                     }
                 }
             })
             .state('app.browse', {
                 url: "/browse",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/browse.html"
                     }
                 }
             })
             .state('app.playlists', {
                 url: "/playlists",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/playlists.html",
                         controller: 'PlaylistsCtrl'
                     }
                 }
             })
             .state('app.category', {
                 url: "/quiz",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/playlist.html"
                     }
                 }
             })
             .state('app.leaderboard', {
                 url: "/leaderboard",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/scoremenu.html",
                         controller: 'LeaderBoardCtrl'
                     }
                 }
             });
         // if none of the above states are matched, use this as the fallback
         $urlRouterProvider.otherwise('/app/playlists');
     });
