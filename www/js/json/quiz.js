var data = [{
    id: 1,
    topicId: 1,
    ans: 1,
    title: 'Count the balls below.',
    operation: '',
    img: [{
        src: 'img/new/1.jpg'
    }, {
        src: 'img/new/1.jpg'
    }, {
        src: 'img/new/1.jpg'
    }, {
        src: 'img/new/1.jpg'
    }, {
        src: 'img/new/1.jpg'
    }, {
        src: 'img/new/1.jpg'
    }],
    choices: [{
        choice: '5'
    }, {
        choice: '6'
    }, {
        choice: '4'
    }]
}, {
    id: 2,
    topicId: 1,
    ans: 2,
    title: 'Count the stars below.',
    operation: '',
    img: [{
        src: 'img/new/2.jpg'
    }, {
        src: 'img/new/2.jpg'
    }, {
        src: 'img/new/2.jpg'
    }],
    choices: [{
        choice: '1'
    }, {
        choice: '2'
    }, {
        choice: '3'
    }]
}, {
    id: 3,
    topicId: 1,
    ans: 0,
    title: 'Add the balloons below. Choose the letter of the correct answer.',
    operation: 'add',
    img: [{
        src: 'img/new/3-1.jpg'
    }, {
        src: 'img/new/3-2.jpg'
    }, {
        src: 'img/new/add.jpg'
    }, {
        src: 'img/new/3-1.jpg'
    }, {
        src: 'img/new/3-2.jpg'
    }],
    choices: [{
        choice: '2 + 2 = 4'
    }, {
        choice: '2 + 2 = 2'
    }, {
        choice: '2 + 2 = 1'
    }]
}, {
    id: 4,
    topicId: 1,
    ans: 2,
    title: 'Add the pencils below. What is the correct answer.',
    operation: 'add',
    img: [{
        src: 'img/new/4.jpg'
    }, {
        src: 'img/new/4.jpg'
    }, {
        src: 'img/new/4.jpg'
    }, {
        src: 'img/new/add.jpg'
    }, {
        src: 'img/new/4.jpg'
    }],
    choices: [{
        choice: '3 + 1 = 3'
    }, {
        choice: '3 + 1 = 5'
    }, {
        choice: '3 + 1 = 4'
    }]
}, {
    id: 5,
    topicId: 1,
    ans: 1,
    title: 'Subtract the following below.',
    operation: 'minus',
    img: [{
        src: 'img/new/5.jpg'
    }, {
        src: 'img/new/5.jpg'
    }, {
        src: 'img/new/minus.jpg'
    }, {
        src: 'img/new/5.jpg'
    }],
    choices: [{
        choice: '2 - 1 = 0'
    }, {
        choice: '2 - 1 = 1'
    }, {
        choice: '2 - 1 = 2'
    }]
}, {
    id: 6,
    topicId: 1,
    ans: 1,
    title: 'Choose the correct answer.',
    operation: 'minus',
    img: [{
        src: 'img/new/6.jpg'
    }, {
        src: 'img/new/6.jpg'
    }, {
        src: 'img/new/6.jpg'
    }, {
        src: 'img/new/minus.jpg'
    }, {
        src: 'img/new/6.jpg'
    }, {
        src: 'img/new/6.jpg'
    }],
    choices: [{
        choice: '3 - 2 = 0'
    }, {
        choice: '3 - 2 = 1'
    }, {
        choice: '3 - 2 = 2'
    }]
}, {
    id: 7,
    topicId: 1,
    ans: 0,
    title: 'Choose the correct answer.',
    operation: 'minus',
    img: [{
        src: 'img/new/7.png'
    }, {
        src: 'img/new/7.png'
    }, {
        src: 'img/new/7.png'
    }, {
        src: 'img/new/7.png'
    }, {
        src: 'img/new/minus.jpg'
    }, {
        src: 'img/new/7.png'
    }],
    choices: [{
        choice: '4 - 1 = 3'
    }, {
        choice: '4 - 1 = 5'
    }, {
        choice: '4 - 1 = 4'
    }]
}, {
    id: 8,
    topicId: 1,
    ans: 1,
    title: 'Choose the correct answer.',
    operation: 'minus',
    img: [{
        src: 'img/new/8.png'
    }, {
        src: 'img/new/minus.jpg'
    }, {
        src: 'img/new/8.png'
    }],
    choices: [{
        choice: '1 - 1 = 1'
    }, {
        choice: '1 - 1 = 0'
    }, {
        choice: '1 - 1 = 2'
    }]
}, {
    id: 9,
    topicId: 1,
    ans: 1,
    title: 'Choose the correct answer.',
    operation: 'minus',
    img: [{
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/minus.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }, {
        src: 'img/new/9.jpg'
    }],
    choices: [{
        choice: '5 - 3 = 4'
    }, {
        choice: '5 - 3 = 2'
    }, {
        choice: '5 - 3 = 3'
    }]
}, {
    id: 10,
    topicId: 1,
    ans: 1,
    title: 'GREATER THAN AND LESS THAN',
    operation: '',
    img: [{
        src: 'img/2.png'
    }, {
        src: 'img/new/less.jpg'
    }, {
        src: 'img/1.png'
    }],
    choices: [{
        choice: 'TRUE'
    }, {
        choice: 'FALSE'
    }]
}, {
    id: 11,
    topicId: 1,
    ans: 0,
    title: 'GREATER THAN AND LESS THAN',
    operation: '',
    img: [{
        src: 'img/5.png'
    }, {
        src: 'img/new/greater.png'
    }, {
        src: 'img/3.png'
    }],
    choices: [{
        choice: 'TRUE'
    }, {
        choice: 'FALSE'
    }]
}, {
    id: 12,
    topicId: 1,
    ans: 1,
    title: 'GREATER THAN AND LESS THAN',
    operation: '',
    img: [{
        src: 'img/4.png'
    }, {
        src: 'img/new/less.jpg'
    }, {
        src: 'img/2.png'
    }],
    choices: [{
        choice: 'TRUE'
    }, {
        choice: 'FALSE'
    }]
}, {
    id: 13,
    topicId: 1,
    ans: 0,
    title: 'GREATER THAN AND LESS THAN',
    operation: '',
    img: [{
        src: 'img/4.png'
    }, {
        src: 'img/new/equal.jpg'
    }, {
        src: 'img/4.png'
    }],
    choices: [{
        choice: 'TRUE'
    }, {
        choice: 'FALSE'
    }]
}];

starter.Values.value('Quiz', {
    quiz: data
});
