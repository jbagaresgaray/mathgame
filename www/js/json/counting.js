var data = [{
    topicId: 4,
    ans: 0,
    title: [{
        img: '1-1.jpg'
    }, {
        img: '1-1.jpg'
    }, {
        img: '1-1.jpg'
    }, {
        img: '1-1.jpg'
    }],
    choices: [{
        choice: '4 (Four)',
        value: 4
    }, {
        choice: '5 (Five)',
        value: 5
    }]
}, {
    topicId: 4,
    ans: 1,
    title: [{
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }, {
        img: '1-2.jpg'
    }],
    choices: [{
        choice: '0 (Zero)',
        value: 0
    }, {
        choice: '7 (Seven)',
        value: 7
    }]
}, {
    topicId: 4,
    ans: 0,
    title: [{
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }, {
        img: '1-3.jpg'
    }],
    choices: [{
        choice: '7 (Seven)',
        value: 7
    }, {
        choice: '9 (Nine)',
        value: 9
    }]
}, {
    topicId: 4,
    ans: 1,
    title: [{
        img: '1-4.jpg'
    }, {
        img: '1-4.jpg'
    }, {
        img: '1-4.jpg'
    }],
    choices: [{
        choice: '5 (Five)',
        value: 5
    }, {
        choice: '3 (Three)',
        value: 3
    }]
}, {
    topicId: 4,
    ans: 0,
    title: [{
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }, {
        img: '1-5.jpg'
    }],
    choices: [{
        choice: '10 (Ten)',
        value: 10
    }, {
        choice: '8 (Eight)',
        value: 8
    }]
}, {
    topicId: 4,
    ans: 0,
    title: [{
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }, {
        img: '1-6.jpg'
    }],
    choices: [{
        choice: '8 (Eight)',
        value: 8
    }, {
        choice: '4 (Four)',
        value: 4
    }]
}, {
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    },{
        img: 'c1.png'
    }],
    choices: [{
        choice: '8 (Eight)',
        value: 8
    }, {
        choice: '10 (Ten)',
        value: 10
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c2.jpg'
    },{
        img: 'c2.jpg'
    },{
        img: 'c2.jpg'
    }],
    choices: [{
        choice: '3 (Three)',
        value: 3
    }, {
        choice: '4 (Four)',
        value: 4
    }]
},{
    topicId: 4,
    ans: 1,
    title: [{
        img: 'c3.jpg'
    },{
        img: 'c3.jpg'
    },{
        img: 'c3.jpg'
    },{
        img: 'c3.jpg'
    }],
    choices: [{
        choice: '3 (Three)',
        value: 3
    }, {
        choice: '4 (Four)',
        value: 4
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c4.jpg'
    },{
        img: 'c4.jpg'
    },{
        img: 'c4.jpg'
    },{
        img: 'c4.jpg'
    },{
        img: 'c4.jpg'
    }],
    choices: [{
        choice: '5 (Five)',
        value: 5
    }, {
        choice: '4 (Four)',
        value: 4
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c5.jpg'
    }],
    choices: [{
        choice: '3 (Three)',
        value: 3
    }, {
        choice: '2 (Two)',
        value: 2
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c6.png'
    }],
    choices: [{
        choice: '4 (Four)',
        value: 4
    }, {
        choice: '3 (Three)',
        value: 3
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c7.png'
    },{
        img: 'c7.png'
    },{
        img: 'c7.png'
    },{
        img: 'c7.png'
    },{
        img: 'c7.png'
    },{
        img: 'c7.png'
    }],
    choices: [{
        choice: '6 (Six)',
        value: 6
    }, {
        choice: '7 (Seven)',
        value: 7
    }]
},{
    topicId: 4,
    ans: 1,
    title: [{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    },{
        img: 'c8.jpg'
    }],
    choices: [{
        choice: '10 (Ten)',
        value: 10
    }, {
        choice: '8 (Eight)',
        value: 8
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c9.jpg'
    }],
    choices: [{
        choice: '1 (One)',
        value: 1
    }, {
        choice: '2 (Two)',
        value: 2
    }]
},{
    topicId: 4,
    ans: 0,
    title: [{
        img: 'c10.jpg'
    }],
    choices: [{
        choice: '10 (Ten)',
        value: 10
    }, {
        choice: '8 (Eight)',
        value: 8
    }]
}];

starter.Values.value('Counting', {
    quiz: data
});
