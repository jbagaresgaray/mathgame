var data = [{
    topicId: 2,
    ans: 2,
    title: 'Identify the shape’s below.',
    img: 'diamond.png',
    choices: [{
        choice: 'TRIANGLE',
        value: 'triangle'
    }, {
        choice: 'SQUARE',
        value: 'square'
    }, {
        choice: 'DIAMOND',
        value: 'diamond'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify the shape’s below.',
    img: 'heart.png',
    choices: [{
        choice: 'TRIANGLE',
        value: 'triangle'
    }, {
        choice: 'HEART',
        value: 'heart'
    }, {
        choice: 'DIAMOND',
        value: 'diamond'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify the shape’s below.',
    img: 'square.jpg',
    choices: [{
        choice: 'TRIANGLE',
        value: 'triangle'
    }, {
        choice: 'SQUARE',
        value: 'square'
    }, {
        choice: 'CIRCLE',
        value: 'circle'
    }]
}, {
    topicId: 2,
    ans: 2,
    title: 'Identify the shape’s below.',
    img: 'circle.png',
    choices: [{
        choice: 'RECTANGLE',
        value: 'rectangle'
    }, {
        choice: 'SQUARE',
        value: 'square'
    }, {
        choice: 'CIRCLE',
        value: 'circle'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify the shape’s below.',
    img: 'c1.png',
    choices: [{
        choice: 'STAR',
        value: 'star'
    }, {
        choice: 'SQUARE',
        value: 'square'
    }, {
        choice: 'TRIANGLE',
        value: 'triangle'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify the shape’s below.',
    img: 'd5.jpg',
    choices: [{
        choice: 'STAR',
        value: 'star'
    }, {
        choice: 'SQUARE',
        value: 'square'
    }, {
        choice: 'TRIANGLE',
        value: 'triangle'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Select the correct fraction from the given choices below',
    img: '_1.png',
    choices: [{
        choice: '1/4'
    }, {
        choice: '1/2'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Select the correct fraction from the given choices below',
    img: '_2.png',
    choices: [{
        choice: '1/2'
    }, {
        choice: '1/4'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Select the correct fraction from the given choices below',
    img: '_3.png',
    choices: [{
        choice: '2/3'
    }, {
        choice: '2/5'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Select the correct fraction from the given choices below',
    img: '_4.png',
    choices: [{
        choice: '1/7'
    }, {
        choice: '1/8'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Select the correct fraction from the given choices below',
    img: '_5.png',
    choices: [{
        choice: '3/4'
    }, {
        choice: '1/5'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify what shape.',
    img: 'd1.jpg',
    choices: [{
        choice: 'CIRCLE'
    }, {
        choice: 'SQUARE'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify what shape.',
    img: 'd2.jpg',
    choices: [{
        choice: 'PARALLELOGRAM'
    }, {
        choice: 'SQUARE'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify what shape.',
    img: 'd3.jpg',
    choices: [{
        choice: 'DIAMOND'
    }, {
        choice: 'CIRCLE'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify what shape.',
    img: 'd4.jpg',
    choices: [{
        choice: 'CIRCLE'
    }, {
        choice: 'OVAL'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify what shape.',
    img: 'd5.jpg',
    choices: [{
        choice: 'STAR'
    }, {
        choice: 'CUBE'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify what shape.',
    img: 'd6.png',
    choices: [{
        choice: 'RECTANGLE'
    }, {
        choice: 'TRAPEZOID'
    }]
}, {
    topicId: 2,
    ans: 1,
    title: 'Identify what shape.',
    img: 'd7.jpg',
    choices: [{
        choice: 'CIRCLE'
    }, {
        choice: 'OCTAGON'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify what shape.',
    img: 'd8.jpg',
    choices: [{
        choice: 'CUBE'
    }, {
        choice: 'CIRVLE'
    }]
}, {
    topicId: 2,
    ans: 0,
    title: 'Identify what shape.',
    img: 'd9.png',
    choices: [{
        choice: 'PENTAGON'
    }, {
        choice: 'DIAMOND'
    }]
}];

starter.Values.value('Drag', {
    quiz: data
});
