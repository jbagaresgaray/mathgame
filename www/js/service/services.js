'use strict';

var CountingFactory = function($http, Counting) {
    var CountingFactory = {};
    CountingFactory.quiz = [];

    CountingFactory.getQuiz = function() {
        CountingFactory.quiz = Counting.quiz
        return CountingFactory.quiz;
    };

    return CountingFactory;
};

var QuizFactory = function($http, Quiz) {
    var QuizFactory = {};
    QuizFactory.quiz = [];

    QuizFactory.getQuiz = function() {
        QuizFactory.quiz = Quiz.quiz
        return QuizFactory.quiz;
    };

    return QuizFactory;
};

var DragFactory = function($http, Drag) {
    var DragFactory = {};
    DragFactory.quiz = [];

    DragFactory.getQuiz = function() {
        DragFactory.quiz = Drag.quiz
        return DragFactory.quiz;
    };

    return DragFactory;
};

var Score = function(DB) {
    var self = this;

    self.all = function() {
        return DB.query('SELECT * FROM scores')
            .then(function(result) {
                return DB.fetchAll(result);
            });
    };

    self.previousScore = function(topicId) {
        return DB.query('SELECT * FROM scores WHERE topicId=? ORDER BY score,id DESC LIMIT 1;', [topicId])
            .then(function(result) {
                return DB.fetch(result);
            });
    };

    self.insert = function(topic, title, context) {
        return DB.query('INSERT INTO scores(topicId,name,score) VALUES (?,?,?)', [topic, title, context])
            .then(function(result) {
            console.log('result: ',result);
                return result.insertId;
            });
    }

    self.byTopic = function(topicId) {
        return DB.query('SELECT * FROM scores WHERE topicId=? ORDER BY score ASC;', [topicId])
            .then(function(result) {
                return DB.fetchAll(result);
            });
    };
    return self;
}

starter.Services.factory('Score', ['DB', Score]);
starter.Services.factory('CountingFactory', ['$http', 'Counting', CountingFactory]);
starter.Services.factory('QuizFactory', ['$http', 'Quiz', QuizFactory]);
starter.Services.factory('DragFactory', ['$http', 'Drag', CountingFactory]);
