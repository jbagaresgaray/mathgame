'use strict';


var AppCtrl = function($scope, $ionicModal, $timeout) {}

var PlaylistsCtrl = function($scope, $ionicPopup, $ionicModal, $ionicLoading, $ionicPlatform, $cordovaMedia) {

    $scope.isPlay = false;
    var media = null;

    document.addEventListener("deviceready", onDeviceReady, false);

    $scope.showConfirm = function() {
        $ionicPopup.show({
            template: "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/panel2.png) center no-repeat;background-size: 100% 100%; }</style>",
            scope: $scope,
            buttons: [{
                type: 'button-close'
            }, {
                type: 'button-check',
                onTap: function() {
                    navigator.app.exitApp()
                }
            }]
        });
    };

    $scope.play = function() {
        if ($scope.isPlay) {
            media.stop();
            $scope.isPlay = false;
        } else {
            $scope.isPlay = true;
            media.play();
        }
    }

    function onDeviceReady() {
        media = $cordovaMedia.newMedia('/android_asset/www/music/ost_game.mp3', function() {
            console.log('Play Success');
        }, function(err) {
            console.log("playAudio():Audio Error: " + err);
        }, function(status) {
            onsole.log('Play status', status);
            if (status == Media.MEDIA_STOPPED) {
                media.play();
                onsole.log('Play again');
            };
        });

        $ionicPlatform.ready(function() {
            $scope.play();
        });
    }
}

var LeaderBoardCtrl = function($scope, $ionicModal, $ionicLoading, $timeout, Score) {
    $ionicModal.fromTemplateUrl('templates/leaderboard.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.closeScores = function() {
        $scope.modal.hide();
    };

    $scope.showScores = function(topic) {
        $scope.scores = {};

        $scope.modal.show();

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            Score.byTopic(topic).then(function(document) {
                console.log(document);
                $scope.scores = document;
            });

            $ionicLoading.hide();
        }, 1000);
    };

}


starter.Controllers.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', AppCtrl]);
starter.Controllers.controller('PlaylistsCtrl', ['$scope', '$ionicPopup', '$ionicModal', '$ionicLoading', '$ionicPlatform', '$cordovaMedia', PlaylistsCtrl]);
starter.Controllers.controller('LeaderBoardCtrl', ['$scope', '$ionicModal', '$ionicLoading', '$timeout', 'Score', LeaderBoardCtrl]);
