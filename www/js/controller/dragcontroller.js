'use strict';

var score;
var questionmade = [];
var questions = [];
var questionnumber;

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i]
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}


var DragDropCtrl = function($scope, $stateParams, $state, $ionicLoading, $timeout, $ionicPopup, DragFactory, Score) {

    init();

    $scope.toggleCenterAnchor = function() {
        $scope.centerAnchor = !$scope.centerAnchor
    }

    $scope.onDropComplete1 = function(data, evt, index) {
        console.log("index: ", index, "$event: ", evt, "onDropComplete1", "", data);

        $scope.selected_image = data.img;
        $scope.index = index;
        var my_ans = index;
        var answer = data.ans;

        $scope.enable = false;

        $scope.data = {
            index: index,
            value: data.ans,
            scope: data
        }

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            $ionicLoading.hide();
            if (my_ans == answer) {
                $scope.eventButton(1);
            } else {
                $scope.eventButton(0);
            }
        }, 1000);

    }

    $scope.eventButton = function(value) {
        $scope.data.clientSide = value;
        console.log($scope.data.clientSide);

        var template = '';

        if (value == 0) {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/wrong.png) center no-repeat;background-size: 100% 100%; }</style>";
        } else {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/correct.png) center no-repeat;background-size: 100% 100%; }</style>";
        }
        $ionicPopup.show({
            template: template,
            scope: $scope,
            buttons: [{
                type: 'button-popnext',
                onTap: function() {
                    $ionicLoading.show({
                        template: 'Loading...'
                    });


                    $timeout(function() {
                        $ionicLoading.hide();
                        var len = questions.length;
                        if (questionnumber != (len - 1)) {
                            questionnumber = questionnumber + 1;

                            questionmade.push($scope.data);

                            $scope.questions = questions[questionnumber];

                            $scope.selected_image = {};
                            $scope.index = {};
                            $scope.enable = false;
                            $scope.prevenable = true;

                            $scope.stopTimer();
                            $scope.startTimer();

                        } else {
                            var result = {
                                topicId: $scope.id
                            };

                            questionmade.push($scope.data);

                            $scope.enable = false;
                            $scope.prevenable = false;

                            window.localStorage['quiz'] = JSON.stringify(questionmade);

                            $state.go('score', result);
                        }
                    }, 300);
                }
            }]
        });
    }

    $scope.onTimeout = function() {
        if ($scope.counter === 0) {
            $scope.$broadcast('timer-stopped', 0);
            $timeout.cancel(mytimeout);
            return;
        }
        $scope.counter--;
        mytimeout = $timeout($scope.onTimeout, 1000);
    };

    $scope.startTimer = function() {
        mytimeout = $timeout($scope.onTimeout, 1000);
    };


    $scope.stopTimer = function() {
        $scope.$broadcast('timer-stopped', $scope.counter);
        $scope.counter = 60;
        $timeout.cancel(mytimeout);
    };

    $scope.$on('timer-stopped', function(event, remaining) {
        if (remaining === 0) {
            console.log('your time ran out!');

            window.localStorage['quiz'] = JSON.stringify(questionmade);

            var result = {
                topicId: $scope.id
            };

            $state.go('score', result);
        }
    });

    function init() {

        window.localStorage['topic'] = '2';
        questionnumber = 0;
        $scope.enable = false;
        score = 0;

        $scope.counter = 60;

        questions = DragFactory.getQuiz() || [];
        questions = questions.splice(0, 10);
        questions = shuffle(questions);

        $scope.data = {
            index: '',
            value: '',
            scope: {}
        };

        $scope.selected_image = {};
        $scope.index = {};
        $scope.centerAnchor = true;

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            var quiz = questions[questionnumber];

            $scope.questions = quiz;

            $scope.startTimer();

            $ionicLoading.hide();
        }, 1000);

    }
}

starter.Controllers.controller('DragDropCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', '$timeout', '$ionicPopup', 'DragFactory', 'Score', DragDropCtrl]);
