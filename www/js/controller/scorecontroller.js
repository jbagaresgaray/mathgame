'use strict';

var ScoreCtrl = function($scope, $timeout, $stateParams, $ionicPopup, $state, $location, Score) {

    var topic = JSON.parse(window.localStorage['topic'] || []);
    var quizarray = [];

    init();

    $scope.saveScore = function() {

        var score = $scope.score + '/' + quizarray.length;

        var template = "<style>.popup-head {border:none;} .popup-buttons {padding:0px !important;}.popup-container .popup { width:295px;height:300px;background: transparent url(img/quiz_complete.png) center no-repeat;background-size: 100% 100%; }</style>" +
            "<div style='margin:20px;'><div class='text-center'><img src='img/star1.png' height='48px'><img src='img/star2.png' height='62px'><img src='img/star3.png' height='48px'> <h2>Score: " + score + "</h2></div>" +
            "<input type='text' class='custom_input_text' placeholder='Enter Name' ng-model='data.username'></div>";

        $ionicPopup.show({
            template: template,
            scope: $scope,
            buttons: [{
                type: 'button-forward',
                onTap: function() {
                    if (!$scope.data.username) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Math Game',
                            template: 'Please enter your name'
                        });
                        return;
                    } else {
                        console.log('topic: ', topic, ' name: ', $scope.data.username, ' score: ', score);
                        Score.insert(topic, $scope.data.username, score).then(function(documents) {
                            console.log(documents);
                            if (documents) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Math Game App',
                                    template: 'Record Successfully Saved'
                                });
                                $timeout(function() {
                                    window.location.href = '#/app';
                                    window.location.reload();
                                }, 1000);
                            }
                        });
                    }
                }
            }]
        });
    }

    $scope.resetQuiz = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Reset Quiz',
            template: 'Are you sure you want to reset this quiz?'
        });
        confirmPopup.then(function(res) {
            if (res) {
                $state.go('app.playlists', {}, {
                    reload: true
                });
                $timeout(function() {
                    window.location.reload();
                }, 20);

                // $ionicHistory.clearHistory();
                // $ionicHistory.clearCache();
            } else {
                return;
            }
        });
    }

    function init() {

        $scope.results = {};
        $scope.score = {};
        $scope.data = {}

        $scope.quiz = {
            title: '',
            answer: '',
            myans: '',
            img: []
        };

        var score = 0;

        if (localStorage.getItem("quiz") != null) {
            var quiz = JSON.parse(localStorage.getItem("quiz") || []);

            if (quiz) {
                for (var i = 0; i < quiz.length; i++) {
                    var myans = quiz[i].index;
                    var mychoice = quiz[i].scope.choices[myans].choice;

                    var ans = quiz[i].scope.ans;
                    var title = quiz[i].scope.title;
                    var choice = quiz[i].scope.choices[ans].choice;
                    var images = quiz[i].scope.img;

                    var iscorrect = false;
                    if (myans == ans) {
                        iscorrect = true;
                        score = score + 1;
                    }

                    $scope.quiz = {
                        title: title,
                        answer: choice,
                        myans: mychoice,
                        correct: iscorrect,
                        img: images
                    };
                    quizarray.push($scope.quiz);

                };
                $scope.results = quizarray;
                $scope.score = score;
                $scope.limit = quizarray.length;
            } else {
                $scope.score = 0;
                $scope.limit = 0;
                $scope.results = [];
            }
        } else {
            $scope.score = 0;
            $scope.limit = 0;
            $scope.results = [];
        }

    }
}

starter.Controllers.controller('ScoreCtrl', ['$scope', '$timeout', '$stateParams', '$ionicPopup', '$state', '$location', 'Score', ScoreCtrl]);
