'use strict';

var ArrangeCtrl = function($scope, $stateParams, $ionicLoading) {

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    var data = [{
        name: 'one',
        img: '1.png'
    }, {
        name: 'two',
        img: '2.png'
    }, {
        name: 'three',
        img: '3.png'
    }, {
        name: 'four',
        img: '4.png'
    }, {
        name: 'five',
        img: '5.png'
    }];

    $scope.draggableObjects = shuffle(data);

    $scope.onDropComplete = function(index, obj, evt) {
        var otherObj = $scope.draggableObjects[index];
        var otherIndex = $scope.draggableObjects.indexOf(obj);
        $scope.draggableObjects[index] = obj;
        $scope.draggableObjects[otherIndex] = otherObj;
    }

        $scope.eventButton = function(value) {
        $scope.data.clientSide = value;
        console.log($scope.data.clientSide);

        var template = '';

        if (value == 0) {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/wrong.png) center no-repeat;background-size: 100% 100%; }</style>";
        } else {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/correct.png) center no-repeat;background-size: 100% 100%; }</style>";
        }
        $ionicPopup.show({
            template: template,
            scope: $scope,
            buttons: [{
                type: 'button-popnext',
                onTap: function() {
                    $ionicLoading.show({
                        template: 'Loading...'
                    });

                    if (value == 0) {
                        $ionicLoading.hide();
                        $scope.quizcomplete();
                    } else {
                        $timeout(function() {
                            $ionicLoading.hide();

                            var len = questions.length;

                            if (questionnumber != (len - 1)) {
                                questionnumber = questionnumber + 1;
                                $scope.selected_image = {};
                                $scope.index = {};
                                $scope.questions = questions[questionnumber];
                            } else {
                                $scope.quizcomplete();
                            }

                        }, 1000);
                    }
                }
            }]
        });
    }


    $scope.quizcomplete = function() {
        var template = "<style>.popup-head {border:none;} .popup-buttons {padding:0px !important;}.popup-container .popup { width:295px;height:300px;background: transparent url(img/quiz_complete.png) center no-repeat;background-size: 100% 100%; }</style>" +
            "<div style='margin:20px;'><div class='text-center'><img src='img/star1.png' height='48px'><img src='img/star2.png' height='62px'><img src='img/star3.png' height='48px'> <h2>Score: " + score + "</h2></div>" +
            "<input type='text' class='custom_input_text' placeholder='Enter Name' ng-model='data.username'></div>";

        $ionicPopup.show({
            template: template,
            scope: $scope,
            buttons: [{
                type: 'button-forward',
                onTap: function() {
                    if (!$scope.data.username) {
                        console.log('sadasd');
                        $timeout(function() {
                            window.location.href = '/';
                        }, 1000);
                    } else {
                        var topic = JSON.parse(window.localStorage['topic'] || {});
                        console.log('topic: ', topic, ' name: ', $scope.data.username, ' score: ', score);
                        Score.insert(topic, $scope.data.username, score).then(function(documents) {
                            console.log(documents);
                            if (documents) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Math Game App',
                                    template: 'Record Successfully Saved'
                                });
                                $timeout(function() {
                                    window.location.href = '#/app';
                                    window.location.reload();
                                }, 1000);
                            }
                        });
                    }
                }
            }]
        });
    }
}

starter.Controllers.controller('ArrangeCtrl', ['$scope', '$stateParams', '$ionicLoading', ArrangeCtrl]);
