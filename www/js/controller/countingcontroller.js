'use strict';

var score;
var questionmade = [];
var questions = [];
var questionnumber;

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i]
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

var CountingCtrl = function($scope, $ionicLoading, $state, $timeout, $ionicPopup, Score, CountingFactory) {

    init();


    $scope.eventButton = function(myanswer, answer, data) {

        var template = '';
        var value;

        $scope.data = {
            index: myanswer,
            value: answer,
            scope: data
        }

        if (myanswer == answer) {
            value = 1;
        } else {
            value = 0;
        }

        $timeout(function() {
            $ionicLoading.hide();

            if (value == 0) {
                template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/wrong.png) center no-repeat;background-size: 100% 100%; }</style>";
            } else {
                template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/correct.png) center no-repeat;background-size: 100% 100%; }</style>";
            }
            $ionicPopup.show({
                template: template,
                scope: $scope,
                buttons: [{
                    type: 'button-popnext',
                    onTap: function() {
                        $ionicLoading.show({
                            template: 'Loading...'
                        });

                        $timeout(function() {
                            var len = questions.length;
                            if (questionnumber != (len - 1)) {
                                questionnumber = questionnumber + 1;

                                questionmade.push($scope.data);

                                $scope.questions = questions[questionnumber];
                                console.log(questionmade);

                                $scope.enable = false;
                                $scope.prevenable = true;

                                $scope.stopTimer();
                                $scope.startTimer();

                            } else {
                                var result = {
                                    topicId: $scope.id
                                };

                                questionmade.push($scope.data);

                                $scope.enable = false;
                                $scope.prevenable = false;

                                window.localStorage['quiz'] = JSON.stringify(questionmade);

                                $state.go('countingscore', result);
                            }

                            $ionicLoading.hide();

                        }, 300);
                    }
                }]
            });

        }, 300);

    }

    $scope.onTimeout = function() {
        if ($scope.counter === 0) {
            $scope.$broadcast('timer-stopped', 0);
            $timeout.cancel(mytimeout);
            return;
        }
        $scope.counter--;
        mytimeout = $timeout($scope.onTimeout, 1000);
    };

    $scope.startTimer = function() {
        mytimeout = $timeout($scope.onTimeout, 1000);
    };


    $scope.stopTimer = function() {
        $scope.$broadcast('timer-stopped', $scope.counter);
        $scope.counter = 60;
        $timeout.cancel(mytimeout);
    };

    $scope.$on('timer-stopped', function(event, remaining) {
        if (remaining === 0) {
            console.log('your time ran out!');

            window.localStorage['quiz'] = JSON.stringify(questionmade);

            var result = {
                topicId: $scope.id
            };

            $state.go('score', result);
        }
    });

    function init() {

        $scope.counter = 60;

        window.localStorage['topic'] = '3';

        questionnumber = 0;

        questions = CountingFactory.getQuiz() || [];
        questions = questions.splice(0, 10);
        questions = shuffle(questions);

        $scope.data = {
            index: '',
            value: '',
            scope: {}
        };

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            $scope.questions = questions[questionnumber];

            $scope.startTimer();

            $ionicLoading.hide();
        }, 1000);
    }
}

starter.Controllers.controller('CountingCtrl', ['$scope', '$ionicLoading', '$state', '$timeout', '$ionicPopup', 'Score', 'CountingFactory', CountingCtrl]);
