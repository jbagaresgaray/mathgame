'use strict';

var score;
var questionmade = [];
var questions = [];
var questionnumber;
var mytimeout = null;

var QuizCtrl = function($scope, $stateParams, $ionicLoading, $timeout, $interval, $ionicPopup, $state, Score, QuizFactory) {


    init();

    $scope.serverSideChange = function(index, item, scope) {
        $scope.data = {
            index: index,
            value: item.choice,
            scope: scope
        }
        $scope.enable = true;
    };

    $scope.getquestion = function() {
        var len = questions.length;

        if (questionnumber != (len - 1)) {

            var my_ans = $scope.data.index;
            var answer = $scope.data.scope.ans;
            $scope.enable = false;

            if (my_ans == answer) {
                $scope.eventButton(1);
            } else {
                $scope.eventButton(0);
            }

        } else {

            var my_ans = $scope.data.index;
            var answer = $scope.data.scope.ans;
            $scope.enable = false;

            if (my_ans == answer) {
                $scope.eventButton(1);
            } else {
                $scope.eventButton(0);
            }
        }

    }

    $scope.previousquestion = function() {
        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            if (questionnumber != 0) {
                questionnumber = questionnumber - 1;
                $scope.questions = questions[questionnumber];
                $scope.prevenable = true;
                $scope.enable = true;
            } else {
                $scope.prevenable = false;
                $scope.enable = true;
            }
            $ionicLoading.hide();
        }, 300);
    }

    $scope.eventButton = function(value) {
        $scope.data.clientSide = value;

        var template = '';

        if (value == 0) {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/wrong.png) center no-repeat;background-size: 100% 100%; }</style>";
        } else {
            template = "<style>.popup-head {border:none;}.popup-container .popup { width:300px;height:150px;background: transparent url(img/correct.png) center no-repeat;background-size: 100% 100%; }</style>";
        }
        $ionicPopup.show({
            template: template,
            scope: $scope,
            buttons: [{
                type: 'button-popnext',
                onTap: function() {
                    $ionicLoading.show({
                        template: 'Loading...'
                    });

                    $timeout(function() {
                        var len = questions.length;
                        if (questionnumber != (len - 1)) {
                            questionnumber = questionnumber + 1;

                            questionmade.push($scope.data);

                            $scope.questions = questions[questionnumber];
                            console.log(questionmade);

                            $scope.enable = false;
                            $scope.prevenable = true;

                            $scope.stopTimer();
                            $scope.startTimer();

                        } else {
                            var result = {
                                topicId: $scope.id
                            };

                            questionmade.push($scope.data);

                            $scope.enable = false;
                            $scope.prevenable = false;

                            window.localStorage['quiz'] = JSON.stringify(questionmade);

                            $state.go('score', result);
                        }

                        $ionicLoading.hide();
                    }, 300);
                }
            }]
        });
    }

    $scope.onTimeout = function() {
        if ($scope.counter === 0) {
            $scope.$broadcast('timer-stopped', 0);
            $timeout.cancel(mytimeout);
            return;
        }
        $scope.counter--;
        mytimeout = $timeout($scope.onTimeout, 1000);
    };

    $scope.startTimer = function() {
        mytimeout = $timeout($scope.onTimeout, 1000);
    };


    $scope.stopTimer = function() {
        $scope.$broadcast('timer-stopped', $scope.counter);
        $scope.counter = 60;
        $timeout.cancel(mytimeout);
    };

    $scope.$on('timer-stopped', function(event, remaining) {
        if (remaining === 0) {
            console.log('your time ran out!');

            window.localStorage['quiz'] = JSON.stringify(questionmade);

            var result = {
                topicId: $scope.id
            };

            $state.go('score', result);
        }
    });

    function init() {

        window.localStorage['topic'] = '1';
        questionnumber = 0;
        $scope.enable = false;
        $scope.prevenable = false;
        $scope.time = "00:00";
        score = 0;

        $scope.counter = 60;


        questions = QuizFactory.getQuiz() || [];
        questions = questions.splice(0, 10);
        questions = shuffle(questions);

        $scope.data = {
            index: '',
            value: '',
            scope: {}
        };

        $ionicLoading.show({
            template: 'Loading...'
        });

        $timeout(function() {
            var quiz = questions[questionnumber];

            $scope.questions = quiz;

            $scope.startTimer();

            $ionicLoading.hide();
        }, 300);
    }

    /*function startTimer() {
        console.log('startTimer');
        var oneMinutes = 20;
        var mins = 0;
        var seconds = 0;
        $scope.time = 0;

        $interval(function() {
            mins = parseInt(oneMinutes / 60)
            seconds = parseInt(oneMinutes % 60);
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $scope.time = mins + ":" + seconds;
            $scope.$apply();
            oneMinutes--;

            if (oneMinutes < 0) {
                var result = {
                    topicId: $scope.id
                };

                $state.go('score', result);
            }
        }, 1000);
    }*/

    function shuffle(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i]
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }
}

starter.Controllers.controller('QuizCtrl', ['$scope', '$stateParams', '$ionicLoading', '$timeout', '$interval', '$ionicPopup', '$state', 'Score', 'QuizFactory', QuizCtrl]);
